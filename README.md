# Slot-based Calling Context Encoding
{Introduction}.

# Code Design
The code is separated into language-dependent (call graph analysis) and -independent (instrumentation) parts, so that to use SCCE for other languages like Java the call graph analysis part can be reused.

# Call Graph Analysis
SCCE first partitions the call graph into an acyclic part and a cyclic part by calculating the strongly connected components (SCCs).

A `.dot` file representing the program's static call graph is required. The format should be parsable by `dot`.